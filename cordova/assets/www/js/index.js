function onPageLoad(){
  document.addEventListener('deviceready', onDeviceReady, false);
}

function onDeviceReady() {
  console.log("deviceready");
  btnShowMessage('deviceready');
}

function btnShowMessage(message){
    //cordova.exec(null,null,"intbirdShowToast","toast",[message]);
    window.showtoast(message);
}

function btnShowWebView(){
    window.go("com.intbird.soft.cordova.WebViewActivity");
}

function onLoadImageFail(message){
    window.showtoast(message);
}

function btnTakePicture(){
    navigator.camera.getPicture(onTakImageSuccess, onLoadImageFail,
     {
     sourceType:Camera.PictureSourceType.CAMERA,
     destinationType: Camera.DestinationType.DATA_URL
     });
}
function onTakImageSuccess(imgUrl){
    var url = "data:image/jpeg;base64," + imgUrl;
    document.getElementById('myImage').src=url;
}

function btnLoadPicture(){
    navigator.camera.getPicture(onLoadImageSuccess, onLoadImageFail,
     {
     sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
     destinationType: Camera.DestinationType.FILE_URI
     });
}
function onLoadImageSuccess(imageUrl){
    document.getElementById('myImage').src=imageUrl;
}




